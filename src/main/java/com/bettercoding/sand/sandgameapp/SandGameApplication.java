package com.bettercoding.sand.sandgameapp;

import javafx.application.Application;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SandGameApplication {
    public static void main(String[] args) {
        Application.launch(SandJavaFXApplication.class, args);
    }
}
